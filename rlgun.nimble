# Package

version = "0.1.0"
author = "RLgunner"
description = "Shooting oriented roguelike"
license = "?"
srcDir = "src"
bin = @["rlgun"]

# Deps
requires "nim >= 1.2.0"
# requires "nico >= 0.2.5"
requires "pararules"
requires "paranim"
requires "paratext"
requires "illwill"
requires "cligen"


task web, "Build for web":
  exec "nimble build -d:emscripten"

task webr, "Build release for web":
  exec "nimble build -d: release -d:emscripten"

task runr, "Run rlgun":
  exec "nimble run -d:release"

task rund, "Run debug":
  exec "nimble run -d:debug"

task term, "Build terminal version":
  exec "nim c -d:terminal src/rlgun.nim"

task termr, "Runs release terminal version":
  exec "nim r -d:release -d:terminal src/rlgun.nim"

task termd, "Runs debug terminal version":
  exec "nim r -d:debug -d:terminal src/rlgun.nim"

# task release, "Builds rlgun for current platform":
#  exec "nim c -d:release -o:rlgun src/rlgun.nim"

# task debug, "Builds debug rlgun for current platform":
#  exec "nim c -d:debug -o:rlgun_debug src/rlgun.nim"

# task web, "Builds rlgun for current web":
#  exec "nim js -d:release -o:rlgun.js src/rlgun.nim"

# task webd, "Builds debug rlgun for current web":
#  exec "nim js -d:debug -o:rlgun.js src/rlgun.nim"
