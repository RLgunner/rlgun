import nimgl/glfw
import types

var stick_memory = Action(kind: None)
var pressed: seq[int32]
var mousePos: (float64, float64)

proc keyCallback*(win: GLFWWindow, key, scancode, action, mods: int32) {.cdecl.} =
  case action
  of GLFW_PRESS: pressed.add(key)
  of GLFW_REPEAT: discard
  of GLFW_RELEASE: discard
  else: discard

proc mousePosCallback*(win: GLFWWindow, xpos: float64, ypos: float64) {.cdecl.} =
  echo (xpos, ypos)
  mousePos = (xpos, ypos)

proc getInput*(): Action =
  let key = (pressed & @[-1.int32])[0]
  pressed = @[]
  if key == -1:
    Action(kind: None)
  elif key == GLFWKey.Escape:
    Action(kind: UI, command: Quit)
  elif key == GLFWKey.Up:
    Action(kind: Move, dir: No)
  elif key == GLFWKey.Down:
    Action(kind: Move, dir: So)
  elif key == GLFWKey.Left:
    Action(kind: Move, dir: We)
  elif key == GLFWKey.Right:
    Action(kind: Move, dir: Ea)
  else: Action(kind: None)

