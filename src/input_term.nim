import illwill
import types

func key_to_action(key: Key): Action =
  case key
  of Key.Up, Key.K: Action(kind: Move, dir: No)
  of Key.Right, Key.L: Action(kind: Move, dir: Ea)
  of Key.Down, Key.J: Action(kind: Move, dir: So)
  of Key.Left, Key.H: Action(kind: Move, dir: We)
  of Key.Space, Key.Dot: Action(kind: UI, command: Quit)
  of Key.Q: Action(kind: UI, command: Quit)
  of Key.One: Action(kind: NewMap, param: 1)
  of Key.Two: Action(kind: NewMap, param: 2)
  of Key.Three: Action(kind: NewMap, param: 3)
  of Key.Four: Action(kind: NewMap, param: 4)
  of Key.Five: Action(kind: NewMap, param: 5)
  of Key.Six: Action(kind: NewMap, param: 6)
  of Key.Seven: Action(kind: NewMap, param: 7)
  of Key.Eight: Action(kind: NewMap, param: 8)
  else: Action(kind: None)

proc input*(): Action =
  key_to_action(getKey())
