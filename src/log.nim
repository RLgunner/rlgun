
type
  Log* = ref seq[Msg]
  Msg* = tuple[text: string, flag: MsgFlag]
  MsgFlag* = enum
    Info, Good, Bad, Debug

var player_log* = new Log

proc log*(text: string, flag=Info) =
  player_log[].add((text, flag))

proc no_debug(msg: Msg): bool =
  msg.flag != Debug

iterator view*(log: Log, entries: int, scrollback=0, debug=false): Msg =
  var
    entry = log[].len - scrollback - 1
    to_show = entries
  while entry > 0 and to_show > 0:
    if log[entry].flag != Debug:
      yield log[entry]
      to_show -= 1
    entry -= 1
  while to_show > 0:
    yield ("", Info)
    to_show -= 1

