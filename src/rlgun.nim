import os, sets, random, times
import types, update, rules, log

const
  title = "RLgun"
  fps = 50
  frame_time = init_duration(milliseconds = 1000.div(fps))

proc game_init() =
  randomize()
  let start_point = (rand(2..22), rand(2..22)).Point
  session.insert(Derived, RedrawTiles, new PointSet)
  session.insert(Derived, Redraw_All, true)
  session.insert(World, Attr.Time, 0)
  session.insert(Player, Attr.Time, 0)
  session.insert(Player, EType, Creature)
  session.insert(Player, Pos, start_point)
  session.insert(Player, Char, '@')
  session.insert(Player, Color, 4)
  session.insert(Player, Name, "P1")
  "HELLO".log

  var newMap = neighbours(start_point)
  for p in newMap:
    newMap.incl(neighbours(p))
  make_map(newMap)


when defined terminal:
  import illwill, input_term
  proc main()
  proc tick()
  proc exit() {.noconv.}

  proc main() =
    illwill_init(fullscreen=true)
    set_control_c_hook(exit)
    hide_cursor()
    game_init()
    while true:
      tick()

  proc tick() =
    let start_time = get_time()
    let action = input()
    case action.kind
    of UI:
      if action.command == Quit: exit()
    else: game_update(action)
    draw()
    let delay = get_time() - start_time + frame_time
    sleep(delay.in_milliseconds.int)

  proc exit() {.noconv.} =
    illwill_deinit()
    show_cursor()
    echo()
    quit(0)
else:
  import nimgl/glfw
  import input
  proc main()

  proc tick() {.cdecl.} =
    let start_time = get_time()
    glfwPollEvents()
    let action = get_input()
    case action.kind:
    of UI:
      if action.command == Quit: window.setWindowShouldClose(true)
    else: game_update(action)
    draw()
    window.swapBuffers()
    let delay = get_time() - start_time + frame_time
    sleep(delay.in_milliseconds.int)

  when defined(emscripten):
    proc emscripten_set_main_loop(f: proc() {.cdecl.}, a: cint, b: bool) {.importc.}

  proc main() =
    doAssert glfwInit()

    glfwWindowHint(GLFWContextVersionMajor, 3)
    glfwWindowHint(GLFWContextVersionMinor, 3)
    glfwWindowHint(GLFWOpenglForwardCompat, GLFW_TRUE) # Used for Mac
    glfwWindowHint(GLFWOpenglProfile, GLFW_OPENGL_CORE_PROFILE)
    glfwWindowHint(GLFWResizable, GLFW_TRUE)

    window = glfwCreateWindow(640.int32, 480.int32)
    if window == nil: quit(-1)
    window.makeContextCurrent()
    glfwSwapInterval(1)
    discard window.setKeyCallback(keyCallback)
    discard window.setCursorPosCallback(mousePosCallback)
    discard window.setFramebufferSizeCallback(resizeFrameCallback)

    game_init()
    var width, height: int32
    window.getFramebufferSize(width.addr, height.addr)
    window.resizeFrameCallback(width, height)
    drawInit()

    when defined(emscripten):
      emscripten_set_main_loop(tick, 0, true)
    else:
      while not window.windowShouldClose:
        tick()

    echo "BYE!"
    window.destroyWindow()
    glfwTerminate()

when isMainModule:
  main()
