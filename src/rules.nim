import sets
import pararules
import types, log

export fire_rules, query_all

type
  Id* = enum
    World, Player, Derived
  Attr* = enum
    Time, Log, Spaces, SeeThru, Walls, RedrawTiles, Redraw_All,
    EType, Pos,
    Visible, Seen, BlockSight, BlockMove,
    MoveDir,
    Char, Color, Name

schema Fact(Id, Attr):
  Time: int
  Log: MessageSeq
  Spaces: PointSetRef
  SeeThru: PointSetRef
  Walls: PointSetRef
  RedrawTiles: PointSetRef
  Redraw_All: bool
  EType: EntityType
  Pos: Point
  Visible: bool
  Seen: bool
  BlockSight: bool
  BlockMove: bool
  MoveDir: Direction
  Char: char
  Color: int
  Name: string

proc redraw_tiles(point: Point)

var (session*, rules*) = init_session_with_rules(Fact, auto_fire = false):
  rule pos(Fact):
    what:
      (id, Pos, point)
    then:
      redraw_tiles(point)
  rule spaces(Fact):
    what:
      (World, Spaces, spaces)
    then:
      var walls = new PointSet
      walls[] = perimeter(spaces[])
      session.insert(Derived, Walls, walls)
      session.insert(Derived, Redraw_All, true)
  rule seethru(Fact):
    what:
      (World, SeeThru, seethru)
  rule walls(Fact):
    what:
      (Derived, Walls, walls)
  rule redraw(Fact):
    what:
      (Derived, RedrawTiles, tiles)
      (Derived, Redraw_All, all)
  rule draw_data(Fact):
    what:
      (id, EType, etype)
      (id, Pos, pos)
      (id, Char, char)
      (id, Color, color)
  rule move(Fact):
    what:
      (Player, Pos, pos)
      (Player, MoveDir, dir)
      (World, Spaces, spaces)
    cond:
      dir != Nowhere
      pos + dir in spaces[]
    then:
      session.insert(Player, Pos, pos + dir)
      session.insert(Player, MoveDir, Nowhere)
      redraw_tiles(pos)
      redraw_tiles(pos + dir)
      ($dir).log

proc redraw_tiles(point: Point) =
  let (tiles, all) = session.query(rules.redraw)
  tiles[].incl(point)

# MAP MAKING
#
proc burrow*(points: varargs[Point, pt]) =
  var
    (spaces) = session.query(rules.spaces)
    (seethru) = session.query(rules.seethru)
  for point in points:
    spaces[].incl(point)
    seethru[].incl(point)

proc makeMap*(pointSet: PointSet) =
  let points = new PointSet
  points[] = pointSet
  session.insert(World, Spaces, points)
  session.insert(World, SeeThru, points)
  session.insert(World, Redraw_All, true)

# Use include because rules can't be exported
when defined terminal:
  include ./rules_1_draw_term
else:
  include ./rules_1_draw
