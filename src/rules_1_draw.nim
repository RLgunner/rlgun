# Drawing using paranim
# Lacks imports as the file is included in rules.nim

import nimgl/glfw
import nimgl/opengl
import paranim/gl, paranim/gl/uniforms, paranim/gl/attributes, paranim/gl/entities
import paranim/math
import paratext, paratext/gl/text
import glm

type Game* = object of RootGame
  deltaTime: float
  totalTime: float
  tile: GLFloat
  winWidth: GLFloat
  winHeight: GLFloat
  viewWidth: GLFloat
  viewHeight: GLFloat
  msgWidth: GLFloat
  msgHeight: GLFloat
  offset: Point
  mouseX: float
  mouseY: float

# Paranim entity collections for drawing
var window*: GLFWWindow
var
  wallEnts: InstancedTwoDEntity
  spaceEnts: InstancedTwoDEntity
  actEnts: seq[InstancedTextEntity]
  msgEnts: seq[InstancedTextEntity]
  game = Game()

const ttf* = staticRead("assets/Roboto-Regular.ttf")
const black = vec4(0f, 0f, 0f, 1f)
var font*: Font[2048]
var textBase: UncompiledTextEntity
var textEntity: InstancedTextEntity

proc textInit*[GameT](game: var GameT, width, height: int, fontSize: float) =
  font = initFont(ttf = ttf, fontHeight = fontSize.float, firstChar = 32, bitmapWidth = 512, bitmapHeight = 512, charCount = 2048)
  textBase = initTextEntity(font)
  textEntity = game.compile(initInstancedEntity(textBase))

proc newText*[GameT](game: GameT, text: string, font: Font, color=black): InstancedTextEntity =
  result = gl.copy(textEntity)
  var x_offset = 0f
  for i, ch in text:
    let bakedChar = font.chars[int(ch) - font.firstChar]
    var e = textBase
    e.crop(bakedChar, x_offset, font.baseline)
    e.color(color)
    result.add(e)
    x_offset += bakedChar.xadvance

proc square*(size: GLfloat): UncompiledTwoDEntity =
  let zero = 0.GLfloat
  initTwoDEntity(@[zero, zero,   size, zero,   zero, size,
                   zero, size,   size, zero,   size, size])

proc fillBlocks(tiles: PointSet, color = vec4(0f, 0f, 0f, 1f)) =
  var base = square(game.tile)
  var group = initInstancedEntity(base)
  group.project(GLfloat(game.viewWidth), GLfloat(game.viewHeight))
  base.color(color)
  for tile in tiles:
    var new = gl.copy(base)
    new.translate(float(tile.x) * game.tile, float(tile.y) * game.tile)
    group.add(new)
  spaceEnts = (game.compile(group))

proc mapUpdate*() =
  let (spaces) = session.query(rules.spaces)
  fillBlocks(spaces[], vec4(0.3f, 0.3f, 0.3f, 1f))
  # fillBlocks(session.query(rules.spaces), vec4(0.8f, 0.8f, 0.8f, 1f))

proc drawUpdate*() =
  actEnts = @[]
  for actor in session.query_all(rules.draw_data):
    actEnts.add(game.newText($actor.char, font))
    let (x, y) = (actor.pos.x, actor.pos.y)
    actEnts[^1].project(game.viewWidth, game.viewHeight)
    actEnts[^1].translate(x.float * game.tile, y.float * game.tile)
  # for msg in messages:
  #   msgEnts.add(game.newText(msg, font))

proc drawInit*() =
  doAssert glInit()
  glEnable(GL_BLEND)
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
  glDisable(GL_CULL_FACE)
  glDisable(GL_DEPTH_TEST)
  glClearColor(0.1f, 0.1f, 0.1f, 1f)
  game.tile = 32
  game.offset = (0, 0)
  textInit(game, game.msgWidth.int, game.msgHeight.int, game.tile)

proc draw*() =
  glClear(GL_COLOR_BUFFER_BIT)
  drawUpdate()
  mapUpdate()
  # Main view
  glViewport(0, 0, GLsizei(game.viewWidth), GLsizei(game.viewHeight))

  var e = spaceEnts
  e.translate(game.offset.x.float * game.tile, game.offset.y.float * game.tile)
  game.render(e)

  for n, actor in actEnts:
    var e = actEnts[n]
    e.translate(game.offset.x.float * game.tile, game.offset.y.float * game.tile)
    game.render(e)

  # Side panel
  # glViewport(GLsizei(game.viewWidth), 0, GLsizei(game.msgWidth), GLsizei(game.msgHeight))
  # for n, line in msgEnts:
  #   var e = line
  #   e.project(float(msgWidth), float(msgHeight))
  #   e.translate(000f, float(n) * float(font.height))
  #   game.render(e)

proc resizeFrameCallback*(win: GLFWWindow, width: int32, height: int32): void {.cdecl.} =
  game.winWidth = float(width)
  game.winHeight = float(height)
  game.viewWidth = float(height)
  game.viewHeight = float(height)
  game.msgWidth = float(width - height)
  game.msgHeight = float(height)

