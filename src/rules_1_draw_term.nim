# Drawing using paranim
# Lacks imports as the file is included in rules.nim

var view_offset = (0, 0).Delta

import illwill
proc draw_tile(buffer: var TerminalBuffer, pos: Point, ch: char)
proc draw_map(buffer: var TerminalBuffer)
proc draw_all(buffer: var TerminalBuffer)
proc draw*()

proc draw_tile(buffer: var TerminalBuffer, pos: Point, ch: char) =
  let draw_pos = pos + view_offset
  if draw_pos.x >= 0 and draw_pos.y >= 0:
    buffer.write(draw_pos.x, draw_pos.y, $ch)

proc draw_map(buffer: var TerminalBuffer) =
  let (walls) = session.query(rules.walls)
  for wall in walls[]:
    buffer.draw_tile(wall, '#')

proc draw_all(buffer: var TerminalBuffer) =
  let entities = session.query_all(rules.draw_data)
  for entity in entities:
    if entity.etype == Creature:
      buffer.draw_tile(entity.pos, entity.char)

proc draw*() =
  var buffer = new_terminal_buffer(terminal_width(), terminal_height())
  buffer.draw_map()
  buffer.draw_all()
  buffer.display()

