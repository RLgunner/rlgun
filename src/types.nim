import hashes, sets, options, sugar
from math import sqrt, sgn

export isSome, isNone, get

proc filter*[T](s: HashSet[T], pred: proc(x: T): bool {.closure.}): HashSet[T] =
  for x in s:
    if pred(x):
      result.incl(x)

iterator filter*[T](s: HashSet[T], pred: proc(x: T): bool {.closure.}): T =
  for x in s:
    if pred(x):
      yield x


type
  Point* {.borrow:`.`.} = distinct tuple [x: int, y: int]
  PointSet* = HashSet[Point]
  PointSetRef* = ref HashSet[Point]
func hash*(p: Point): Hash {.borrow.}
func `==`*(p, q: Point): bool {.borrow.}
func `$`*(p: Point): string {.borrow.}
converter pt*(tup: (int, int)): Point = tup.Point

type Delta* {.borrow:`.`.} = distinct tuple [dx: int, dy: int]
func `==`*(p, q: Delta): bool {.borrow.}
func `$`*(p: Delta): string {.borrow.}
func lenSq*(d: Delta): int = d.dx * d.dx + d.dy * d.dy
func len*(d: Delta): float = (d.dx * d.dx + d.dy * d.dy).float.sqrt
func `+`*(p: Point, d: Delta): Point = (p.x + d.dx, p.y + d.dy).Point
func `-`*(p, q: Point): Delta = (p.x - q.x, p.y - q.y).Delta
func `*`*(d: Delta, n: int): Delta = (d.dx * n, d.dy * n).Delta
func `/`*(d: Delta, n: int): Delta = (d.dx div n, d.dy div n).Delta
iterator `..`*(p, q: Point): Point =
  for x in min(p.x, q.x)..max(p.x, q.x):
    for y in min(p.y, q.y)..max(p.y, q.y):
      yield(x, y)


type
  EntityType* = enum
    Creature, Item, Fixture
  TextFormat = tuple[color: int, bold: bool]
  Note = tuple[text: string, format: TextFormat]
  Message* = ref seq[Note]
  MessageSeq* = ref seq[Message]

  Direction* = enum
    Nowhere, No, NE, Ea, SE, So, SW, We, NW

  AI* = enum
    Pass, Human, Basic
  Control* = object
    ai*: AI
    nextTurn*: int

  ActionKind* = enum
    Move, Wait, NewMap, UI, None
  UI_Command* = enum
    Quit
  Action* = object
    case kind*: ActionKind
    of Move: dir*: Direction
    of Wait: discard
    of NewMap: param*: int
    of UI: command*: UI_Command
    of None: discard

  # Don't think I need these.
  #Tile* = tuple
  #  space: bool
  #  clear: bool
  #  seen: bool

  #MapArray[W, H: static[int]] =
  #  array[1..W, array[1..H, Tile]]

func `==`*(action1: Action, action2: Action): bool =
  if action1.kind == action2.kind:
    if action1.kind == Move and action1.dir == action2.dir: true
    elif action1.kind == UI and action1.command == action2.command: true
    else: true
  else: false

func `-`*(dir: Direction): Direction =
  case dir:
    of Nowhere: Nowhere
    of No: So
    of NE: SW
    of Ea: We
    of SE: NW
    of So: No
    of SW: NE
    of We: Ea
    of NW: SE

converter toVector*(dir: Direction): Delta =
  case dir:
    of Nowhere: (0, 0).Delta
    of No: ( 0, -1).Delta
    of NE: ( 1, -1).Delta
    of Ea: ( 1,  0).Delta
    of SE: ( 1,  1).Delta
    of So: ( 0,  1).Delta
    of SW: (-1,  1).Delta
    of We: (-1,  0).Delta
    of NW: (-1, -1).Delta

converter toDir*(d: Delta): Direction =
  if d == (0, 0).Delta: return Nowhere
  if abs(d.dx) >= abs(2 * d.dy) and d.dx > 0: return Ea
  if abs(d.dx) >= abs(2 * d.dy) and d.dx < 0: return We
  if abs(d.dy) >= abs(2 * d.dx) and d.dy > 0: return So
  if abs(d.dy) >= abs(2 * d.dx) and d.dy < 0: return No
  if d.dx > 0 and d.dy < 0: return NE
  if d.dx > 0 and d.dy > 0: return SE
  if d.dx < 0 and d.dy > 0: return SW
  if d.dx < 0 and d.dy < 0: return NW


const dirset = [No, NE, Ea, SE, So, SW, We, NW, No].toHashSet

func concatMap[T,S](s: HashSet[T], f: T -> HashSet[S]): HashSet[S] =
  for x in s:
    result.incl(f(x))

func neighbours*(p: Point): PointSet =
  map(dirset, d => p + d)

func neighbourhood*(ps: PointSet): PointSet =
  concatMap(ps, neighbours)

func perimeter*(ps: PointSet): PointSet =
  neighbourhood(ps) - ps
