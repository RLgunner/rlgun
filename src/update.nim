import pararules
import types, rules, maze_gen/maze

proc gameUpdate*(action: Action) =
  case action.kind:
  of Move: session.insert(Player, MoveDir, action.dir)
  of NewMap: makeMap(mazeMaker(action.param))
  else: discard
  session.fire_rules()
